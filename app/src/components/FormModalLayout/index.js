/**
*
* FormModalLayout
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { Content, Text, Button, Form, Label, Item, Input } from 'native-base';

class FormModalLayout extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  handleSubmit = (values) => {
    const {
      onSubmit,
    } = this.props;
    onSubmit(values);
  }
  render() {
    // console.log(this.props);
    const {
      children,
      record,
    } = this.props;
    return (
      <Content>
        <Formik
          initialValues={record}
          render={({
            setFieldValue,
            values,
            errors,
            touched,
          }) => (
            <Form>
              {React.Children.map(children, child => {
                const {
                  name,
                  ...rest 
                } = child.props;
                return React.cloneElement(child, {
                  onChange: (value) => setFieldValue(name, value),
                  value: values[name],
                  touched: touched[name],
                  error: errors[name],
                  ...rest,
                });
              })}
              <Button onPress={() => this.handleSubmit(values)}><Text>Submit</Text></Button>
            </Form>
          )}
        />
      </Content>
    );
  }
}

FormModalLayout.propTypes = {

};

export default FormModalLayout;
