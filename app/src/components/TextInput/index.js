/**
*
* TextInput
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Input, Item, Label } from 'native-base';

class TextInput extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      name,
      onChange,
      touched,
      error,
      value,
    } = this.props;
    return (
      <Item>
        <Label>{name}</Label>
        <Input onChangeText={onChange} value={value}/>
        {touched && error && <div>{error}</div>}
      </Item>
    );
  }
}

TextInput.propTypes = {

};

export default TextInput;
