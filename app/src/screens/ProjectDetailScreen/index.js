/**
*
* ProjectDetailScreen
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Container, Text } from 'native-base';

class ProjectDetailScreen extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Container>
        <Text>This is ProjectDetailScreen screen</Text>
      </Container>
    );
  }
}

ProjectDetailScreen.propTypes = {

};

export default ProjectDetailScreen;
