/**
*
* TaskDetailScreen
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Container, Text } from 'native-base';

class TaskDetailScreen extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Container>
        <Text>This is TaskDetailScreen screen</Text>
      </Container>
    );
  }
}

TaskDetailScreen.propTypes = {

};

export default TaskDetailScreen;
