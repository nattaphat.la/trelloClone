/**
*
* ProjectListScreen
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Container, Text } from 'native-base';

class ProjectListScreen extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Container>
        <Text>This is ProjectListScreen screen</Text>
      </Container>
    );
  }
}

ProjectListScreen.propTypes = {

};

export default ProjectListScreen;
