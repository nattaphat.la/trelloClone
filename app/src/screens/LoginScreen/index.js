/**
*
* LoginScreen
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
// import { Container, Text } from 'native-base';
import Login from '../../containers/Login';
import FormModalLayout from '../../components/FormModalLayout';
import TextInput from '../../components/TextInput';

class LoginScreen extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Login>
        <FormModalLayout initialValues={{
          username: 'test',
          password: 'testpassword',
          myid: 123,
        }}>
          <TextInput name="username"/>
          <TextInput name="password"/>
        </FormModalLayout>
      </Login>
    );
  }
}

LoginScreen.propTypes = {

};

export default LoginScreen;
