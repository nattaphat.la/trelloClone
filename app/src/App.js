import React, { PureComponent } from "react";
import { Root } from "native-base";
import { createSwitchNavigator, createStackNavigator } from 'react-navigation'
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import ProjectDetailScreen from './screens/ProjectDetailScreen';
import ProjectListScreen from './screens/ProjectListScreen';
import TaskDetailScreen from './screens/TaskDetailScreen';

const AuthStack = createStackNavigator({
  login: LoginScreen,
  registerScreen: RegisterScreen,
}, {
  headerMode: 'none',
})
const AppStack = createStackNavigator({
  projectList: ProjectListScreen,
  projectDetail: ProjectDetailScreen,
  taskDetail: TaskDetailScreen,
})

const AppNavigation = createSwitchNavigator({
  auth: {
    screen: AuthStack
  },
  app: {
    screen: AppStack
  }
})

class App extends PureComponent {
  state = {  }
  render() {
    return (
      <Root>
        <AppNavigation />
      </Root>
    );
  }
}

export default App;