/**
*
* Edit
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Container, Text } from 'native-base';
import { connect } from 'react-redux';

class Edit extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Container>
        <Text>This is Edit container</Text>
      </Container>
    );
  }
}

Edit.propTypes = {

};
const mapStateToProps = state => ({
  // isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => ({
  // login: dispatch.auth.login
});

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
