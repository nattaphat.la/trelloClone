/**
*
* Show
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
import { Container, Text } from 'native-base';
import { connect } from 'react-redux';

class Show extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Container>
        <Text>This is Show container</Text>
      </Container>
    );
  }
}

Show.propTypes = {

};
const mapStateToProps = state => ({
  // isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => ({
  // login: dispatch.auth.login
});

export default connect(mapStateToProps, mapDispatchToProps)(Show);
