/**
*
* Login
*
*/

import React from 'react';
// import styled from 'styled-components';
// import PropTypes from 'prop-types';
// import { Container, Text } from 'native-base';
import { connect } from 'react-redux';

class Login extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  submitHandler = (credential) => {
    const {
      login,
    } = this.props;
    console.log(credential);
    login(credential);
  }
  render() {
    const {
      children,
    } = this.props;
    return (
      React.cloneElement(children, {
        onSubmit: this.submitHandler,
      })
    );
  }
}

Login.propTypes = {

};
const mapStateToProps = state => ({
  // isAuthenticated: state.auth.isAuthenticated,
});

const mapDispatchToProps = dispatch => ({
  login: dispatch.Auth.loginAsync
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
