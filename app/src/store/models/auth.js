export const Auth = {
  state: {
    token: false,
  }, // initial state
  reducers: {
    // handle state changes with pure functions
    setToken(state, payload) {
      return {
        ...state,
        token: payload
      }
    }
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions
    async loginAsync(payload, rootState) {
      const token = await new Promise(resolve => setTimeout(() => resolve('Some token'), 1000))
      dispatch.Auth.setToken(token)
    }
  })
}