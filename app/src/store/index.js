import { init } from '@rematch/core'
import * as models from './models'
import createLoadingPlugin from '@rematch/loading'
import createRematchPersist from '@rematch/persist'
import storage from 'redux-persist/es/storage';

// see options API below
const options = { asNumber: true }
const loading = createLoadingPlugin(options)
const persistPlugin = createRematchPersist({
  storage,
  whitelist: ['Auth'],
  throttle: 5000,
  version: 1,
})
const store = init({
  models,
  plugins: [
    loading,
    persistPlugin,
  ]
})

export default store