'use strict';

module.exports = async function(app, cb) {
  /*
   * The `app` object provides access to a variety of LoopBack resources such as
   * models (e.g. `app.models.YourModelName`) or data sources (e.g.
   * `app.datasources.YourDataSource`). See
   * http://docs.strongloop.com/display/public/LB/Working+with+LoopBack+objects
   * for more info.
   */
  try {
    const dataSource = app.datasources.db;
    console.log('prepare migration');
    const isActual = await (new Promise(resolve =>
      dataSource.isActual(isActual => resolve(isActual)))
    );
    if (isActual) {
      console.log('datasource already match');
      return cb();
    }
    console.log('migration start');
    await (new Promise(resolve =>
      dataSource.autoupdate(() => resolve())
    ));
    console.log('migration end');
    cb();
  } catch (e) {
    return cb(e);
  }
  // process.nextTick(cb); // Remove if you pass `cb` to an async function yourself
};
